/**
 * Created by IvanP on 16.05.2016.
 */
var tree = {
    0: 'user0',
    1: 'user1',
    2: 'user2',
    3: 'user3',
    4: 'user4',
    5: 'user5',
    6: 'user6',
    7: 'user7',
    8: 'user8',
    9: 'user9'
};

var myObject =[ {
    title:  'Frog',
    url:    '/img/picture.jpg',
    width:  300,
    height: 200
},
{
    title:  'Pig',
    url:    '/img/picture.jpg',
    width:  300,
    height: 200
}];

var content;
function list_tree(){
    content += '<ul>';
    for(var i in myObject){
        content += '<li style="cursor: pointer" data-param="1">'+ myObject[i]['title'] +'</li>';
    }
    content += '</ul>';
    return content;
}

$("#building").html(list_tree());